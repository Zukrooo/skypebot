﻿using SKYPE4COMLib;
using System;
using System.Text.RegularExpressions;

namespace SkypeBot
{
  class MessageHandler
  {
    private IUser botUser_;

    private CommandHandler handler_;

    private bool wasMessage_ = false;

    public MessageHandler(IUser botUser, CommandHandler handler)
    {
      botUser_ = botUser;
      handler_ = handler;
    }

    public void process(ChatMessage pMessage, TChatMessageStatus Status)
    {
      if (Status == TChatMessageStatus.cmsSending || Status == TChatMessageStatus.cmsReceived)
      {
        if (wasMessage_ == true)
        {
          Console.WriteLine(">>>> " + pMessage.Sender.FullName + ": " + pMessage.Body);
          wasMessage_ = true;
        }
        else if (wasMessage_ == false)
        {
          Console.WriteLine("");
          Console.WriteLine("Message:");
          Console.WriteLine(">>>> " + pMessage.Sender.FullName + ": " + pMessage.Body);
          wasMessage_ = true;
        }
        //Checks for potential command
        if (pMessage.Body.StartsWith("%"))
        {
          Console.WriteLine("");
          Console.WriteLine("Command:");
          Console.WriteLine(">>>> " + pMessage.Sender.FullName + ": Command recognized");
          handler_.create(pMessage.Chat, pMessage.Body.Substring(1), pMessage.Sender.FullName);
          wasMessage_ = false;
        }
        else if (Regex.Match(pMessage.Body, YouTubeTitleRetrieverCommand.YoutubeMask).Success)
        {
          Console.WriteLine("");
          Console.WriteLine("Command:");
          Console.WriteLine(">>>> " + pMessage.Sender.FullName + ": YouTube link recognized!");
          handler_.create(pMessage.Chat, pMessage.Body, pMessage.Sender.FullName);
          wasMessage_ = false;
        }
      }
    }
  }
}
