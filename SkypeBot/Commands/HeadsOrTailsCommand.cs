﻿using SKYPE4COMLib;
using System;

namespace SkypeBot
{
  public class HeadsOrTailsCommand : Command
  {
    private Chat chat_;
    private string message_;

    private int randnum;

    private string sender_;

    public HeadsOrTailsCommand(Chat chat, string message, string sender)
    {
      chat_ = chat;
      message_ = message;
      sender_ = sender;
    }

    public void process()
    {
      string[] strings = message_.Split(new char[] { ' ' });

      if (strings.Length > 1)
      {
        message_ = strings[1];
      }

      switch (message_.ToLower())
      {
        case "heads":
        case "tails":
          Random rand = new Random();
          randnum = rand.Next(0, 2);

          chat_.SendMessage("HoT: " + sender_ + " has chosen " + message_ + ". Flipping the Coin...");

          System.Threading.Thread.Sleep(2500);

          if (randnum == 0)
          {
            chat_.SendMessage("HoT: Unfortunately, " + sender_ + ", You lost this one.");
          }
          else
          {
            chat_.SendMessage("HoT: Congratulations, " + sender_ + ", You won this one!");
          }
          break;
        default:
          chat_.SendMessage("HoT: The syntax for this command is '%hot <heads or tails>'.");
          break;
      }
    }

    public string getName()
    {
      return "Heads or Tails";
    }
  }
}
