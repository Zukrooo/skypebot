﻿using SKYPE4COMLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkypeBot
{
  class SearchCommand : Command
  {
    private const string googleUrl = "http://www.google.com/search?q={0}";
    private const string youtubeUrl = "https://www.youtube.com/results?search_query={0}";

    private string message_;
    private Chat chat_;
    private string sender_;

    public SearchCommand(Chat chat, string message, string sender)
    {
      chat_ = chat;
      message_ = message;
      sender_ = sender;
    }

    public void process()
    {
      string searchQuery = message_.Substring(message_.IndexOf(' ') + 1).Replace(" ", "+");

      chat_.SendMessage(String.Format("Google: " + googleUrl + Environment.NewLine
                                    + "YouTube: " + youtubeUrl, new object[] { searchQuery }));
    }

    public string getName()
    {
      return "GoogleSearchQuery";
    }
  }
}
