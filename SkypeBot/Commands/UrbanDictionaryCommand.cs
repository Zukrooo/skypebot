﻿using SKYPE4COMLib;
using System;
using System.Net;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Linq;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace SkypeBot
{
  class UrbanDictionaryDefinition
  {
    public string definition { get; set; }
  }

  class UrbanDictionaryCommand : Command
  {
    private Chat chat_;
    private string message_;
    private string sender_;

    private const string link = "http://api.urbandictionary.com/v0/define?term=";

    public UrbanDictionaryCommand(Chat chat, string message, string sender)
    {
      chat_ = chat;
      message_ = message;
      sender_ = sender;
    }

    public void process()
    {
      string phrase = message_.Substring( message_.IndexOf( ' ' ) + 1 );

      string htmlEncodedPhrase = System.Web.HttpUtility.HtmlEncode(phrase);
      string escapedPhrase = Uri.EscapeDataString(htmlEncodedPhrase);

      string urlString = link + escapedPhrase;

      WebClient webClient = new WebClient();
      string data = webClient.DownloadString(urlString);

      Match error = Regex.Match(data, "{\"result_type\":\"no_results\"}");

      if (error.Success)
      {
        chat_.SendMessage("UrbanDictionary: No results found.");
        return;
      }
      else
      {
        var definition = JsonConvert.DeserializeObject<UrbanDictionaryDefinition>(data);

        chat_.SendMessage( String.Format( "UrbanDictionary: {0}", new object[]{  } ) );
      }
    }

    public string getName()
    {
      return "UrbanDictionaryRetriever";
    }
  }
}
