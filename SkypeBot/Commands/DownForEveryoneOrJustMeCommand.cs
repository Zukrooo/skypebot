﻿using SKYPE4COMLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using HtmlAgilityPack;

namespace SkypeBot
{
  class DownForEveryoneOrJustMeCommand : Command
  {
    private Chat chat_;
    private string message_;
    private string sender_;

    private const string urlString = "http://www.isup.me/";
    private string linkToCheck;
    private string finalURL;

    public DownForEveryoneOrJustMeCommand(Chat chat, string message, string sender)
    {
      chat_ = chat;
      message_ = message;
      sender_ = sender;
    }

    public void process()
    {
      string linkToCheck = message_.Substring(message_.IndexOf(' ') + 1);

      finalURL = urlString + linkToCheck;

      WebClient client = new WebClient();

      Stream stream = client.OpenRead(finalURL);
      StreamReader reader = new StreamReader(stream);
      string page = reader.ReadToEnd();

      HtmlAgilityPack.HtmlDocument document = new HtmlAgilityPack.HtmlDocument();
      document.LoadHtml(page);

      string result = document.GetElementbyId("container").InnerText;

      if (result.Contains("It's just you."))
      {
        chat_.SendMessage("IsUp: " + linkToCheck + " is up.");
      }
      else if (result.Contains("It's not just you!"))
      {
        chat_.SendMessage("IsUp: " + linkToCheck + " looks down from here.");
      }
      else if (result.Contains("Huh?"))
      {
        chat_.SendMessage("IsUp: " + linkToCheck + " doesn't appear to exist.");  
      }
    }

    public string getName()
    {
      return "DownForEveryoneOrJustMe";
    }
  }
}
