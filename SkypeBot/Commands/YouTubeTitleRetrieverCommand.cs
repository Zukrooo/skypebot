﻿using SKYPE4COMLib;
using System;
using System.Net;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Linq;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace SkypeBot
{
  class YoutubeVideoDetails
  {
    [JsonProperty("title")]
    public string Title { get; set; }

    [JsonProperty("duration")]
    public string Length { get; set; }

    [JsonProperty("likeCount")]
    public int Likes { get; set; }

    [JsonProperty("ratingCount")]
    public int Dislikes { get; set; }
  }

  public class YouTubeTitleRetrieverCommand : Command
  {
    private const string link_ = "http://gdata.youtube.com/feeds/api/videos/{0}?v=2&alt=jsonc";
    public const string YoutubeMask = @"(https?://(www\.)?)?youtube\.com/watch?(.[^\s\r\n&]*)?&?v=(.[^\s\r\n&]*)";
    private string message_;
    private Chat chat_;

    public YouTubeTitleRetrieverCommand(Chat chat, string message)
    {
      chat_ = chat;
      message_ = message;
    }

    public void process()
    {
      Match id = Regex.Match(message_, YoutubeMask);

      string url = String.Format(link_, id.Groups[4]);
      string data = "";

      try
      {
        WebClient webClient = new WebClient();
        data = webClient.DownloadString(url);
      }
      catch (Exception e)
      {
        chat_.SendMessage("YouTube: Video does not exist.");
      }

      if (data.Length > 0)
      {
        JObject root = JObject.Parse(data);

        var video = JsonConvert.DeserializeObject<YoutubeVideoDetails>(root["data"].ToString());

        video.Dislikes = video.Dislikes - video.Likes;

        chat_.SendMessage(String.Format("YouTube: {0} - {1:c}, Likes: {2}, Dislikes {3}", new object[] { video.Title, new TimeSpan(0, 0, Int32.Parse(video.Length)), video.Likes, video.Dislikes }));
      }
    }

    public string getName()
    {
      return "YoutubeRetriever";
    }
  }
}
