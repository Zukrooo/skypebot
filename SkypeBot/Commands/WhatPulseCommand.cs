﻿using SKYPE4COMLib;
using System.Linq;
using System.Xml.Linq;
using System.Net;
using System.Web;
using System;
using System.Text.RegularExpressions;
using Newtonsoft.Json;

namespace SkypeBot
{
  class WhatPulseUser
  {
    public string AccountName { get; set; }
    public string Keys { get; set; }
    public string Clicks { get; set; }
  }

  public class WhatPulseCommand : Command
  {
    private Chat chat_;
    private string message_;
    private string sender_;

    private string urlString_;

    public WhatPulseCommand(Chat chat, string message, string sender)
    {
      chat_ = chat;
      message_ = message;
      sender_ = sender;
    }

    public void process()
    {
      string username = message_.Substring( message_.IndexOf( ' ' ) + 1 );

      if ( username.Length > 35 ) {
        chat_.SendMessage( "WhatPulse: Username must not contain more than 35 characters" );
        return;
      }

      string htmlEncodedUsername = System.Web.HttpUtility.HtmlEncode(username);
      string escapedUsername = Uri.EscapeDataString(htmlEncodedUsername);

      urlString_ = "http://api.whatpulse.org/user.php?formatted=yes&format=json&user=" + escapedUsername;

      WebClient webClient = new WebClient();
      string data = webClient.DownloadString(urlString_);

      Match error = Regex.Match(data, "{\"error\":\"(.*)\"}");

      if (error.Success)
      {
        chat_.SendMessage("WhatPulse: " + error.Groups[1]);
        return;
      }
      else
      {
        var user = JsonConvert.DeserializeObject<WhatPulseUser>(data);

        chat_.SendMessage( String.Format( "WhatPulse: {0} - Keys: {1} Clicks: {2}", new object[]{ username, user.Keys, user.Clicks } ) );
      }
    }

    public string getName()
    {
      return "WhatPulse";
    }
  }
}
