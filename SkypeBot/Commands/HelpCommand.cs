﻿using SKYPE4COMLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkypeBot
{
  class HelpCommand : Command
  {
    private Chat chat_;
    private string message_;
    private string sender_;

    public HelpCommand(Chat chat, string message, string sender)
    {
      chat_ = chat;
      message_ = message;
      sender_ = sender;
    }

    public void process()
    {
      chat_.SendMessage("Here are the commands supported by this SkypeBot:" + Environment.NewLine 
                      + "Commands Should be entered at the start of the message" + Environment.NewLine
                      + "  Date and Time (%date or %time)" + Environment.NewLine
                      + "  Definition Retriever (%def <word or phrase>)" + Environment.NewLine
                      + "  UrbanDictionary Definition (%urb <word or phrase>) [Not implemented]" + Environment.NewLine
                      + "  Heads Or Tails (%hot <heads or tails>)" + Environment.NewLine
                      + "  WhatPulse Stats retriever (%wp <username>)" + Environment.NewLine
                      + "  Check if a website is up (%isup <website url>)" + Environment.NewLine
                      + "  Google/YouTube Search (%search <word or phrase>)" + Environment.NewLine
                      + "  Make me say something (%say <phrase>)" + Environment.NewLine
                      + "  List things (%list <item1> <item2>...)" + Environment.NewLine
                      + "  Automatically retrieves YouTube Video information");
    }

    public string getName()
    {
      return "HelpCommand";
    }
  }
}
