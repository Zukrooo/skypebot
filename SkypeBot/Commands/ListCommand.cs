﻿using SKYPE4COMLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkypeBot
{
  class ListCommand : Command
  {
    private Chat chat_;
    private string message_;
    private string sender_;

    private string list = "";
    private int number = 1;

    public ListCommand(Chat chat, string message, string sender)
    {
      chat_ = chat;
      message_ = message;
      sender_ = sender;
    }

    public void process()
    {
      string[] listItems = message_.Split(new char[] { ' ' });

      list += "List: " + Environment.NewLine;
      for (int i = 1; i < listItems.Length; i++)
      {
        list += "  " + number + ". " + listItems[i] + Environment.NewLine;
        number++;
      }

      chat_.SendMessage(list);
    }

    public string getName()
    {
      return "ListCommand";
    }
  }
}
