﻿using SKYPE4COMLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkypeBot
{
  class CommandTemplate : Command
  {
    private Chat chat_;
    private string message_;
    private string sender_;

    public CommandTemplate(Chat chat, string message, string sender)
    {
      chat_ = chat;
      message_ = message;
      sender_ = sender;
    }

    public void process()
    { 
    }

    public string getName()
    {
      return "CommandTemplate";
    }
  }
}
