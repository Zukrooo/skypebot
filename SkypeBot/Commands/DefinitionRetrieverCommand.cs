﻿using SKYPE4COMLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Text.RegularExpressions;

namespace SkypeBot
{
  class DefinitionRetrieverCommand : Command
  {
    private Chat chat_;
    private string message_;
    private string sender_;

    public DefinitionRetrieverCommand(Chat chat, string message, string sender)
    {
      chat_ = chat;
      message_ = message;
      sender_ = sender;
    }

    public void process()
    {

    }

    public string getName()
    {
      return "DefinitionRetriever";
    }
  }
}
