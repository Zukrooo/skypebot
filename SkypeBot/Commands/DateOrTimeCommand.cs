﻿using SKYPE4COMLib;
using System;
using System.Net;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Linq;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace SkypeBot
{
  class DateOrTimeCommand : Command
  {
    private Chat chat_;
    private string message_;
    private string sender_;

    public DateOrTimeCommand(Chat chat, string message, string sender)
    {
      chat_ = chat;
      message_ = message;
      sender_ = sender;
    }

    public static string suffix(int num)
    {
      if (num.ToString().EndsWith("11")) return "th";
      if (num.ToString().EndsWith("12")) return "th";
      if (num.ToString().EndsWith("13")) return "th";
      if (num.ToString().EndsWith("1")) return "st";
      if (num.ToString().EndsWith("2")) return "nd";
      if (num.ToString().EndsWith("3")) return "rd";
      return "th";
    }

    public void process()
    {
      string ordinal = suffix(Convert.ToInt32(DateTime.Now.Day));

      DateTime time = DateTime.Now;
      string dateFormat = @"Da\te: " + "dddd, MMMM d";
      string timeFormat = @", yyyy" + Environment.NewLine + @"Ti\me: " + "h:mm:ss tt";

      chat_.SendMessage(time.ToString(dateFormat) + ordinal + time.ToString(timeFormat));
    }

    public string getName()
    {
      return "DateTime";
    }
  }
}
