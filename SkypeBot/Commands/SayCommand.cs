﻿using SKYPE4COMLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkypeBot
{
  class SayCommand : Command
  {
    private Chat chat_;
    private string message_;
    private string sender_;

    public SayCommand(Chat chat, string message, string sender)
    {
      chat_ = chat;
      message_ = message;
      sender_ = sender;
    }

    public void process()
    {
      chat_.SendMessage("Say: " + message_.Substring(message_.IndexOf(' ') + 1) + "By: " + sender_);
    }

    public string getName()
    {
      return "SayCommand";
    }
  }
}
