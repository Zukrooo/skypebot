﻿
namespace SkypeBot
{
  public interface Command
  {
    void process();

    string getName();
  }
}
