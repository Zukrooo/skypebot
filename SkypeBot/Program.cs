﻿using System;
using SKYPE4COMLib;

namespace SkypeBot
{
  class Program
  {
    static void Main(string[] args)
    {
      Skype skype = new Skype();
      skype.Attach(skype.Protocol, true);

      CommandHandler commandHandler = new CommandHandler();
      MessageHandler messageHandler = new MessageHandler(skype.CurrentUser, commandHandler);
      SkypeEventHandler handler = new SkypeEventHandler(messageHandler);
      skype.MessageStatus += handler.MessageStatus;
      
      Console.WriteLine(" ---------------------------------------- ");
      Console.WriteLine("|                SpengBot                |");
      Console.WriteLine("|                                        |");
      Console.WriteLine("|     Developed by Loggie and Zukrooo    |");
      Console.WriteLine(" ---------------------------------------- ");

      while (true)
      {
      }
    }
  }
}
