﻿using SKYPE4COMLib;
using System;
using System.Reflection;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.IO;

namespace SkypeBot
{
  class CommandHandler
  {
    public struct command
    {
      public string trigger;
      public Command classFile;
    }

    public void create(Chat chat, string message, string sender)
    {
      Command cmd = null;
      string[] strings = message.ToLower().ToString().Split(new char[] { ' ' });

      if (Regex.Match(message, YouTubeTitleRetrieverCommand.YoutubeMask).Success)
      {
        Console.WriteLine(sender + ": Getting 'YouTube' video title.");
        cmd = new YouTubeTitleRetrieverCommand(chat, message);
      }
      else
      {
        Dictionary<int, command> commands = new Dictionary<int, command>
        {
          {0, new command {trigger = "hot", classFile = new HeadsOrTailsCommand(chat, message, sender)}},
          {1, new command {trigger = "wp", classFile =  new WhatPulseCommand(chat, message, sender)}},
          {2, new command {trigger = "def", classFile = new DefinitionRetrieverCommand(chat, message, sender)}},
          {3, new command {trigger = "urb", classFile = new UrbanDictionaryCommand(chat, message, sender)}},
          {4, new command {trigger = "isup", classFile = new DownForEveryoneOrJustMeCommand(chat, message, sender)}},
          {5, new command {trigger = "time", classFile = new DateOrTimeCommand(chat, message, sender)}},
          {6, new command {trigger = "date", classFile = new DateOrTimeCommand(chat, message, sender)}},
          {7, new command {trigger = "say", classFile = new SayCommand(chat, message, sender)}},
          {8, new command {trigger = "search", classFile = new SearchCommand(chat, message, sender)}},
          {9, new command {trigger = "help", classFile = new HelpCommand(chat, message, sender)}},
          {10, new command {trigger = "list", classFile = new ListCommand(chat, message, sender)}},
        };
        foreach (KeyValuePair<int, command> pair in commands)
        {
          if (strings[0] == pair.Value.trigger.ToString())
          {
            cmd = pair.Value.classFile;
            break;
          }
        }
      }

      if (cmd != null)
      {
        Console.WriteLine(">>>> " + sender + ": Processing " + cmd.getName());

        try
        {
          cmd.process();
        }
        catch (Exception e)
        {
          Console.WriteLine("Unhandled Exception Processing Command: {0}", cmd.getName());
          Console.WriteLine(e.ToString());

          string filePath = @"C:\Users\" + Environment.UserName + @"\Desktop\SpengBotLogs\";
          string timeDate = DateTime.Now.ToString("-dd-MM-yy-HH-mm-ss");
          string fileName = String.Format(cmd.getName() + "{0}", timeDate);

          if (Directory.Exists(filePath).Equals(false))
          {
            Directory.CreateDirectory(filePath);
          }

          string[] fileContents;
          fileContents = new string[8];
          fileContents[0] = DateTime.Now.ToString("dddd, MMMM d, yyyy - h:mm:ss tt");
          fileContents[1] = "Command called:";
          fileContents[2] = cmd.getName();
          fileContents[3] = "---------------------------------------------------------";
          fileContents[4] = "Command Information";
          fileContents[5] = sender + ": " + message;
          fileContents[6] = "---------------------------------------------------------";
          fileContents[7] = e.ToString();

          File.WriteAllLines(filePath + fileName + ".botlog", fileContents);
        }
      }
      else
      { 
        chat.SendMessage("What the hell kind of command was that? Do '%help' for a list of commands."); 
      }
    }
  }
}
