﻿using SKYPE4COMLib;
using SkypeBot;
using System;

partial class SkypeEventHandler : _ISkypeEvents
{
  private MessageHandler messageHandler_;

  public SkypeEventHandler( MessageHandler handler )
  {
    if (handler == null) throw new NullReferenceException("handler");

    messageHandler_ = handler;
  }

  public void UnhandledEvent(string name)
  {
    Console.WriteLine("Unhandled Event: " + name);
  }

  public void ApplicationConnecting(Application pApp, UserCollection pUsers)
  {
    UnhandledEvent("ApplicationConnecting");
  }

  public void ApplicationDatagram(Application pApp, ApplicationStream pStream, string Text)
  {
    UnhandledEvent("ApplicationDatagram");
  }

  public void ApplicationReceiving(Application pApp, ApplicationStreamCollection pStreams)
  {
    UnhandledEvent("ApplicationReceiving");
  }

  public void ApplicationSending(Application pApp, ApplicationStreamCollection pStreams)
  {
    UnhandledEvent("ApplicationSending");
  }

  public void ApplicationStreams(Application pApp, ApplicationStreamCollection pStreams)
  {
    UnhandledEvent("ApplicationStreams");
  }

  public void AsyncSearchUsersFinished(int Cookie, UserCollection pUsers)
  {
    UnhandledEvent("AsyncSearchUsersFinished");
  }

  public void AttachmentStatus(TAttachmentStatus Status)
  {
    UnhandledEvent("AttachmentStatus");
  }

  public void AutoAway(bool Automatic)
  {
    UnhandledEvent("AutoAway");
  }

  public void CallDtmfReceived(Call pCall, string code)
  {
    UnhandledEvent("CallDtmfReceived");
  }

  public void CallHistory()
  {
    UnhandledEvent("CallHistory");
  }

  public void CallInputStatusChanged(Call pCall, bool Status)
  {
    UnhandledEvent("CallInputStatusChanged");
  }

  public void CallSeenStatusChanged(Call pCall, bool Status)
  {
    UnhandledEvent("CallSeenStatusChanged");
  }

  public void CallStatus(Call pCall, TCallStatus Status)
  {
    UnhandledEvent("CallStatus");
  }

  public void CallTransferStatusChanged(Call pCall, TCallStatus Status)
  {
    UnhandledEvent("CallTransferStatusChanged");
  }

  public void CallVideoReceiveStatusChanged(Call pCall, TCallVideoSendStatus Status)
  {
    UnhandledEvent("CallVideoReceiveStatusChanged");
  }

  public void CallVideoSendStatusChanged(Call pCall, TCallVideoSendStatus Status)
  {
    UnhandledEvent("CallVideoSendStatusChanged");
  }

  public void CallVideoStatusChanged(Call pCall, TCallVideoStatus Status)
  {
    UnhandledEvent("CallVideoStatusChanged");
  }

  public void ChatMemberRoleChanged(IChatMember pMember, TChatMemberRole Role)
  {
    UnhandledEvent("ChatMemberRoleChanged");
  }

  public void ChatMembersChanged(Chat pChat, UserCollection pMembers)
  {
    UnhandledEvent("ChatMembersChanged");
  }

  public void Command(SKYPE4COMLib.Command pCommand)
  {
    UnhandledEvent("Command");
  }

  public void ConnectionStatus(TConnectionStatus Status)
  {
    UnhandledEvent("ConnectionStatus");
  }

  public void ContactsFocused(string Username)
  {
    UnhandledEvent("ContactsFocused");
  }

  public void Error(SKYPE4COMLib.Command pCommand, int Number, string Description)
  {
    UnhandledEvent("Error");
  }

  public void FileTransferStatusChanged(IFileTransfer pTransfer, TFileTransferStatus Status)
  {
    UnhandledEvent("FileTransferStatusChanged");
  }

  public void GroupDeleted(int GroupId)
  {
    UnhandledEvent("GroupDeleted");
  }

  public void GroupExpanded(Group pGroup, bool Expanded)
  {
    UnhandledEvent("GroupExpanded");
  }

  public void GroupUsers(Group pGroup, UserCollection pUsers)
  {
    UnhandledEvent("GroupUsers");
  }

  public void GroupVisible(Group pGroup, bool Visible)
  {
    UnhandledEvent("GroupVisible");
  }

  public void MessageHistory(string Username)
  {
    UnhandledEvent("MessageHistory");
  }

  public void MessageStatus(ChatMessage pMessage, TChatMessageStatus Status)
  {
    messageHandler_.process(pMessage, Status);
  }

  public void Mute(bool Mute)
  {
    UnhandledEvent("Mute");
  }

  public void OnlineStatus(User pUser, TOnlineStatus Status)
  {
    UnhandledEvent("OnlineStatus");
  }

  public void PluginEventClicked(PluginEvent pEvent)
  {
    UnhandledEvent("PluginEventClicked");
  }

  public void PluginMenuItemClicked(PluginMenuItem pMenuItem, UserCollection pUsers, TPluginContext PluginContext, string ContextId)
  {
    UnhandledEvent("PluginMenuItemClicked");
  }

  public void Reply(SKYPE4COMLib.Command pCommand)
  {
    UnhandledEvent("Reply");
  }

  public void SilentModeStatusChanged(bool Silent)
  {
    UnhandledEvent("SilentModeStatusChanged");
  }

  public void SmsMessageStatusChanged(SmsMessage pMessage, TSmsMessageStatus Status)
  {
    UnhandledEvent("SmsMessageStatusChanged");
  }

  public void SmsTargetStatusChanged(SmsTarget pTarget, TSmsTargetStatus Status)
  {
    UnhandledEvent("SmsTargetStatusChanged");
  }

  public void UILanguageChanged(string code)
  {
    UnhandledEvent("UILanguageChanged");
  }

  public void UserAuthorizationRequestReceived(User pUser)
  {
    UnhandledEvent("UserAuthorizationRequestReceived");
  }

  public void UserMood(User pUser, string MoodText)
  {
    UnhandledEvent("UserMood");
  }

  public void UserStatus(TUserStatus Status)
  {
    UnhandledEvent("UserStatus");
  }

  public void VoicemailStatus(Voicemail pMail, TVoicemailStatus Status)
  {
    UnhandledEvent("VoicemailStatus");
  }

  public void WallpaperChanged(string Path)
  {
    UnhandledEvent("WallpaperChanged");
  }
}